/*
 * File: 20180608181654-create-especialidades.js
 *
 * Created: 08 jun 2018 13:34:43
 * Last Modified: 08 jun 2018 14:46:43
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Especialidades', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      especialidad: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        unique: true,
        type: Sequelize.STRING
      },
      descripcion: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.TEXT
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.dropTable('Especialidades');
  }
};
