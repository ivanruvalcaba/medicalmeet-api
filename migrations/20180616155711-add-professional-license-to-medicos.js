/*
 * File: 20180616155711-add-professional-licence-to-medicos.js
 *
 * Created: 16 jun 2018 11:11:05
 * Last Modified: 17 jun 2018 08:48:32
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Medicos', 'cedula_profesional', {
      type: Sequelize.STRING
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn( 'Medicos', 'cedula_profesional' );
  }
};
