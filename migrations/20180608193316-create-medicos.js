/*
 * File: 20180608193316-create-medicos.js
 *
 * Created: 08 jun 2018 14:47:29
 * Last Modified: 17 jun 2018 08:56:12
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Medicos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.STRING
      },
      telefono: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.STRING
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING
      },
      horario: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.TEXT
      },
      duracion_consulta: {
        allowNull: false,
        validate: {
          isInt: true,
          notEmpty: true
        },
        type: Sequelize.INTEGER
      },
      observaciones: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.TEXT
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.dropTable('Medicos');
  }
};
