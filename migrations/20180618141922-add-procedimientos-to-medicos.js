/*
 * File: 20180618141922-add-procedimientos-to-medicos.js
 *
 * Created: 18 jun 2018 09:22:09
 * Last Modified: 18 jun 2018 09:22:09
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Medicos', 'procedimientos', {
      type: Sequelize.TEXT
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn( 'Medicos', 'procedimientos' );
  }
};
