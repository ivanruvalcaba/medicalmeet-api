/*
 * File: 20180609221838-add-medicos-citas-association.js
 *
 * Created: 09 jun 2018 17:23:11
 * Last Modified: 09 jun 2018 17:23:11
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Citas', 'medico_id', {
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
      references: {
        model: 'Medicos',
        key: 'id'
      },
      type: Sequelize.INTEGER
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn('Citas', 'medico_id');
  }
};
