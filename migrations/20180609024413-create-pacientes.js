/*
 * File: 20180609024413-create-pacientes.js
 *
 * Created: 08 jun 2018 22:30:30
 * Last Modified: 25 jul 2018 08:31:06
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pacientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nombre: {
        allowNull: false,
        validate: {
          notEmpty: true,
        },
        type: Sequelize.STRING,
      },
      domicilio: {
        allowNull: false,
        validate: {
          notEmpty: true,
        },
        type: Sequelize.TEXT,
      },
      foraneo: {
        allowNull: true,
        defaultValue: false,
        type: Sequelize.BOOLEAN,
      },
      telefono: {
        allowNull: false,
        validate: {
          notEmpty: true,
        },
        type: Sequelize.STRING,
      },
      email: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      alergias: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      observaciones: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    // eslint-disable-line no-unused-vars
    return queryInterface.dropTable('Pacientes');
  },
};
