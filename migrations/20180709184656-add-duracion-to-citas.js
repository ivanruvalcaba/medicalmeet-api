/*
 * File: 20180709184656-add-duracion-to-citas.js
 *
 * Created: 09 jul 2018 13:51:35
 * Last Modified: 09 jul 2018 13:51:35
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Citas', 'duracion', {
      type: Sequelize.INTEGER
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn( 'Citas', 'duracion' );
  }
};
