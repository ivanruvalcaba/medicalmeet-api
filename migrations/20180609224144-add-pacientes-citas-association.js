/*
 * File: 20180609224144-add-pacientes-citas-association.js
 *
 * Created: 09 jun 2018 17:45:21
 * Last Modified: 09 jun 2018 17:45:21
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Citas', 'paciente_id', {
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
      references: {
        model: 'Pacientes',
        key: 'id'
      },
      type: Sequelize.INTEGER
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn('Citas', 'paciente_id');
  }
};
