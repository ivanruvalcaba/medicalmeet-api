/*
 * File: 20180609025104-create-citas.js
 *
 * Created: 08 jun 2018 22:52:58
 * Last Modified: 14 jun 2018 09:01:30
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Citas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      consulta: {
        allowNull: true,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      fecha: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.DATEONLY
      },
      hora: {
        allowNull: false,
        validate: {
          notEmpty: true
        },
        type: Sequelize.TIME
      },
      observaciones: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      confirmada: {
        allowNull: true,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      presente: {
        allowNull: true,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.dropTable('Citas');
  }
};
