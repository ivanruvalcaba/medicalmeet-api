/*
 * File: 20180609141022-add-especialidades-medicos-association.js
 *
 * Created: 09 jun 2018 09:26:38
 * Last Modified: 09 jun 2018 09:28:30
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Medicos', 'especialidad_id', {
      onDelete: 'SET NULL',
      onUpdate: 'CASCADE',
      references: {
        model: 'Especialidades',
        key: 'id'
      },
      type: Sequelize.INTEGER
    });
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.removeColumn('Medicos', 'especialidad_id');
  }
};
