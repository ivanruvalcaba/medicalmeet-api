/*
 * File: db.js
 *
 * Created: 20 may 2018 22:56:26
 * Last Modified: 21 may 2018 09:04:16
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

let db = null;

module.exports = app => {
  if (!db) {
    const config = app.libs.config;
    const dir = path.join(__dirname, 'models');
    const sequelize = new Sequelize(
      config.database,
      config.username,
      config.password,
      config.params
    );

    db = {
      sequelize,
      Sequelize,
      models: {}
    };

    fs.readdirSync(dir).forEach(file => {
      const modelDir = path.join(dir, file);
      const model = sequelize.import(modelDir);

      db.models[model.name] = model;
    });

    Object.keys(db.models).forEach(key => {
      db.models[key].associate(db.models);
    });
  }

  return db;
};
