[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <mario.i.ruvalcaba[at]gmail[dot]com>)
[//]: # (Created: 10 jun 2018 01:07:10)
[//]: # (Last Modified: 10 jun 2018 01:07:10)

# MedicalMeet API

> API for MedicalMeet — An application for medical appointment management.

## License

* Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>.
* Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>.

This software is covered under the **Mozilla Public License Version 2.0** (_MPL-2.0_) — [Learn more](https://choosealicense.com/licenses/mpl-2.0/).
