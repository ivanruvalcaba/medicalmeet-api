/*
 * File: middlewares.js
 *
 * Created: 20 may 2018 22:03:52
 * Last Modified: 01 jun 2018 21:08:40
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

import bodyParser from 'body-parser';

module.exports = app => {
  app.set('port', 3000);
  app.set('json spaces', 4);

  app.use(bodyParser.json());
  app.use((req, res, next) => { // eslint-disable-line no-unused-vars
    delete req.body.id;
    next();
  });
};
