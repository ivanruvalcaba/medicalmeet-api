/*
 * File: config.js
 *
 * Created: 20 may 2018 22:51:52
 * Last Modified: 20 may 2018 22:51:52
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  database: 'medicalmeet_development',
  username: 'root',
  password: '',
  host: '127.0.0.1',
  params: {
    dialect: 'mysql',
    define: {
      underscored: true
    }
  }
};
