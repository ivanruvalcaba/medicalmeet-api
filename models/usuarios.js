/*
 * File: usuarios.js
 *
 * Created: 08 jun 2018 22:55:47
 * Last Modified: 04 jul 2018 12:12:47
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = (sequelize, DataTypes) => {
  const Usuarios = sequelize.define('Usuarios', {
    nombre: DataTypes.STRING,
    telefono: DataTypes.STRING,
    email: DataTypes.STRING,
    usuario: DataTypes.STRING,
    contrasena: DataTypes.STRING,
    tipo_usuario: {
      allowNull: true,
      type: DataTypes.INTEGER
    }
  }, {
    underscored: true,
    classMethods: {
      associate: (models) => {
        Usuarios.hasMany(models.Citas);
      }
    }
  });
  return Usuarios;
};
