/*
 * File: citas.js
 *
 * Created: 01 jun 2018 12:11:06
 * Last Modified: 09 jul 2018 13:54:21
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = (sequelize, DataTypes) => {
  const Citas = sequelize.define('Citas', {
    consulta: DataTypes.BOOLEAN,
    fecha: DataTypes.DATE,
    hora: DataTypes.TIME,
    observaciones: DataTypes.TEXT,
    confirmada: DataTypes.BOOLEAN,
    presente: DataTypes.BOOLEAN,
    duracion: {
      allowNull: false,
      type: DataTypes.INTEGER,
      validate: {
        notEmpty: true
      }
    }
  }, {
    underscored: true,
    classMethods: {
      associate: (models) => {
        Citas.belongsTo(models.Usuarios, {
          foreignKey: 'usuario_id'
        });
      }
    }
  });
  return Citas;
};
