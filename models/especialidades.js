/*
 * File: especialidades.js
 *
 * Created: 20 may 2018 21:36:33
 * Last Modified: 10 jun 2018 10:46:03
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = (sequelize, DataTypes) => {
  const Especialidades = sequelize.define('Especialidades', {
    especialidad: DataTypes.STRING,
    descripcion: DataTypes.TEXT
  }, {
    underscored: true,
    classMethods: {
      associate: (models) => {  // eslint-disable-line no-unused-vars
      }
    }
  });
  return Especialidades;
};
