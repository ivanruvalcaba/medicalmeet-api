/*
 * File: pacientes.js
 *
 * Created: 01 jun 2018 12:51:20
 * Last Modified: 09 jun 2018 17:46:54
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = (sequelize, DataTypes) => {
  const Pacientes = sequelize.define('Pacientes', {
    nombre: DataTypes.STRING,
    domicilio: DataTypes.TEXT,
    foraneo: DataTypes.BOOLEAN,
    telefono: DataTypes.STRING,
    email: DataTypes.STRING,
    alergias: DataTypes.TEXT,
    observaciones: DataTypes.TEXT
  }, {
    underscored: true,
    classMethods: {
      associate: (models) => {
        Pacientes.belongsToMany(models.Medicos, {
          foreignKey: 'paciente_id',
          through: {
            model: models.Citas,
            unique: false
          }
        });
      }
    }
  });
  return Pacientes;
};
