/*
 * File: medicos.js
 *
 * Created: 21 may 2018 00:01:17
 * Last Modified: 01 jul 2018 00:35:27
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = (sequelize, DataTypes) => {
  const Medicos = sequelize.define('Medicos', {
    nombre: DataTypes.STRING,
    telefono: DataTypes.STRING,
    email: DataTypes.STRING,
    horario: DataTypes.TEXT,
    duracion_consulta: DataTypes.INTEGER,
    observaciones: DataTypes.TEXT,
    cedula_profesional: {
      allowNull: true,
      type: DataTypes.STRING
    },
    procedimientos: {
      allowNull: true,
      type: DataTypes.TEXT
    }
  }, {
    underscored: true,
    classMethods: {
      associate: (models) => {
        Medicos.belongsTo(models.Especialidades, {
          foreignKey: 'especialidad_id'
        });
        Medicos.belongsToMany(models.Pacientes, {
          foreignKey: 'medico_id',
          through: {
            model: models.Citas,
            unique: false
          }
        });
      }
    }
  });
  return Medicos;
};
