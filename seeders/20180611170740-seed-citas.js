/*
 * File: 20180611170740-seed-citas.js
 *
 * Created: 11 jun 2018 22:52:16
 * Last Modified: 14 jun 2018 09:11:32
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    function getNewDate() {
      const minDay = Math.ceil(1);
      const maxDay = Math.floor(30);
      const days = Math.floor(Math.random() * (maxDay - minDay)) + minDay;
      const currentDate = new Date();
      const newDate = new Date(currentDate);

      newDate.setDate(newDate.getDate() + days);

      return new Date(newDate.getFullYear(), newDate.getMonth(),
        newDate.getDate(), 0, 0, 0).toISOString().split('T')[0];
    }

    function getNewTime() {
      const minMinutes = Math.ceil(1);
      const maxMinutes = Math.floor(59);
      const minutes = Math.floor(
        Math.random() * (maxMinutes - minMinutes)) + minMinutes;
      const currentDate = new Date();
      const newMinutes = new Date(currentDate);
      const newHour = new Date(currentDate);

      newHour.setTime(newHour.getTime() + minutes * 3600000);
      newMinutes.setTime(newMinutes.getTime() + minutes * 60000);

      return new Date(currentDate.getFullYear(), currentDate.getMonth(),
        currentDate.getDate(), newHour.getHours(),
        newMinutes.getMinutes(), 0).toISOString().slice(11, 19);
    }

    return queryInterface.bulkInsert('Citas', [{
      consulta: 1,
      fecha: getNewDate(),
      hora: getNewTime(),
      observaciones: 'Ninguna',
      confirmada: false,
      presente: false,
      medico_id: 1,
      paciente_id: 1,
      created_at: new Date(),
      updated_at: new Date()
    }, {
      consulta: 0,
      fecha: getNewDate(),
      hora: getNewTime(),
      observaciones: 'Ninguna',
      confirmada: false,
      presente: false,
      medico_id: 1,
      paciente_id: 1,
      created_at: new Date(),
      updated_at: new Date()
    }, {
      consulta: 1,
      fecha: getNewDate(),
      hora: getNewTime(),
      observaciones: 'Ninguna',
      confirmada: false,
      presente: false,
      medico_id: 1,
      paciente_id: 2,
      created_at: new Date(),
      updated_at: new Date()
    }, {
      consulta: 1,
      fecha: getNewDate(),
      hora: getNewTime(),
      observaciones: 'Ninguna',
      confirmada: false,
      presente: false,
      medico_id: 2,
      paciente_id: 1,
      created_at: new Date(),
      updated_at: new Date()
    }, {
      consulta: 0,
      fecha: getNewDate(),
      hora: getNewTime(),
      observaciones: 'Ninguna',
      confirmada: false,
      presente: false,
      medico_id: 2,
      paciente_id: 1,
      created_at: new Date(),
      updated_at: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkDelete('Citas', null, {});
  }
};
