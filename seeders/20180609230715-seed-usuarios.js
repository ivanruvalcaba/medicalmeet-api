/*
 * File: 20180609230715-seed-usuarios.js
 *
 * Created: 09 jun 2018 18:14:53
 * Last Modified: 09 jun 2018 18:14:53
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkInsert('Usuarios', [{
      nombre: 'MedicalMeet',
      telefono: null,
      email: null,
      usuario: 'medicalmeet',
      contrasena: 'medicalmeet',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      nombre: 'Iván Ruvalcaba',
      telefono: '9531759290',
      email: 'mario.i.ruvalcaba@gmail.com',
      usuario: 'ivanruvalcaba',
      contrasena: 'ivanruvalcaba',
      created_at: new Date(),
      updated_at: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkDelete('Usuarios', null, {});
  }
};
