/*
 * File: 20180609230721-seed-pacientes.js
 *
 * Created: 09 jun 2018 18:26:54
 * Last Modified: 09 jun 2018 18:26:54
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkInsert('Pacientes', [{
      nombre: 'John Doe',
      domicilio: 'Gotham City',
      foraneo: true,
      telefono: '555-HA-HA-HA',
      email: 'thejoker@arkham.org',
      alergias: 'Batman.',
      observaciones: 'Ninguna.',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      nombre: 'Mario Iván Ruvalcaba Vargas',
      domicilio: 'Narnia',
      foraneo: false,
      telefono: '9531759290',
      email: 'mario.i.ruvalcaba@gmail.com',
      alergias: 'Ninguna.',
      observaciones: 'Ninguna.',
      created_at: new Date(),
      updated_at: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkDelete('Pacientes', null, {});
  }
};
