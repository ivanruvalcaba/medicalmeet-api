/*
 * File: 20180609191126-seed-especialidades.js
 *
 * Created: 09 jun 2018 14:58:13
 * Last Modified: 09 jun 2018 15:17:13
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkInsert('Especialidades', [{
      especialidad: 'Medicina Interna',
      descripcion: 'Padecimientos agudos de pacientes adultos, diabetes, hipertensión, digestivos, de la sangre, riñón, corazón, pulmón, síndrome metabólico, obesidad y valoraciones pre-operatorias.',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      especialidad: 'Pediatría',
      descripcion: 'Todos los padecimientos agudos en niños y adolescentes menores de 18 años, atención del RN sano y de alto riesgo, crecimiento y desarrollo, mal nutrición infantil, traumatismos, heridas, prematurez, ictericia, alimentación, etc. Infecciones respiratorias agudas y cuadros diarreicos agudos o crónicos.',
      created_at: new Date(),
      updated_at: new Date()
    }, {
      especialidad: 'Endocrinología',
      descripcion: 'Enfermedades relacionadas con el funcionamiento de las glándulas endocrinas como diabetes,sobrepeso, hipertiroidismo, hipotiroidismo, menopausia, alt., del crecimiento y desarrollo de la infancia y adolescencia, alt., glándulas suprarrenales, paratiroides, hipófisis, etc.',
      created_at: new Date(),
      updated_at: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkDelete('Especialidades', null, {});
  }
};
