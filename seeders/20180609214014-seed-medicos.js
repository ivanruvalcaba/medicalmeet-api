/*
 * File: 20180609214014-seed-medicos.js
 *
 * Created: 09 jun 2018 16:59:52
 * Last Modified: 09 jun 2018 17:03:45
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = {
  up: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkInsert('Medicos', [{
      nombre: 'Luz Aurora León Legaría',
      telefono: '9535555555',
      email: null,
      horario: 'Lunes a viernes de 10:00 a 14:30 y de 18:00 a 18:30. Sábados de 10:00 a 14:30.',
      duracion_consulta: 30,
      observaciones: 'Citar cada 30 minutos. No agendar citas para entrega de resultados, solicitarle al paciente que se presente entre las 14:00 y 15:00 hrs. para tal fin.',
      created_at: new Date(),
      updated_at: new Date(),
      especialidad_id: 1
    }, {
      nombre: 'Pablo Elías López Gutiérrez',
      telefono: '9535555555',
      email: null,
      horario: 'Sábados de 10:00 a 14:30.',
      duracion_consulta: 20,
      observaciones: 'Ninguna.',
      created_at: new Date(),
      updated_at: new Date(),
      especialidad_id: 2
    }], {});
  },

  down: (queryInterface, Sequelize) => {  // eslint-disable-line no-unused-vars
    return queryInterface.bulkDelete('Medicos', null, {});
  }
};
