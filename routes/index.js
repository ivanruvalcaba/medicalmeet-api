/*
 * File: index.js
 *
 * Created: 20 may 2018 19:21:28
 * Last Modified: 01 jun 2018 21:07:32
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = app => {
  app.get('/', (req, res) => {  // eslint-disable-line no-unused-vars
    res.json({
      status: 'MedicalMeet API'
    });
  });
};
