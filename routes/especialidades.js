/*
 * File: especialidades.js
 *
 * Created: 20 may 2018 19:35:30
 * Last Modified: 01 jun 2018 20:47:38
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = app => {
  const Especialidades = app.db.models.Especialidades;

  app.route('/especialidades')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      Especialidades.findAll({})
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .post((req, res) => {
      Especialidades.create(req.body)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/especialidades/:id')
    .get((req, res) => {
      Especialidades.findOne({ where: req.params })
        .then(result => {
          if (result) {
            res.json(result);
          }
          else {
            res.sendStatus(404);
          }
        })
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .put((req, res) => {
      Especialidades.update(req.body, { where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .delete((req, res) => {
      Especialidades.destroy({ where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });
};
