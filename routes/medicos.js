/*
 * File: medicos.js
 *
 * Created: 21 may 2018 09:20:59
 * Last Modified: 09 jul 2018 14:01:48
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = app => {
  const Medicos = app.db.models.Medicos;
  const Pacientes = app.db.models.Pacientes;

  app.route('/medicos')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      Medicos.findAll({})
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .post((req, res) => {
      Medicos.create(req.body)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/medicos/:id')
    .get((req, res) => {
      Medicos.findOne({ where: req.params })
        .then(result => {
          if (result) {
            res.json(result);
          }
          else {
            res.sendStatus(404);
          }
        })
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .put((req, res) => {
      Medicos.update(req.body, { where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .delete((req, res) => {
      Medicos.destroy({ where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/agenda/:id')
    .get((req, res) => {
      app.db.sequelize.query('SELECT Medicos.id AS medico_id, Medicos.nombre AS medico_nombre, Medicos.duracion_consulta AS duracion_consulta, Pacientes.id, Pacientes.nombre, Citas.fecha, Citas.hora, Citas.duracion FROM Medicos INNER JOIN Citas ON Medicos.id = Citas.medico_id INNER JOIN Pacientes ON Pacientes.id = Citas.paciente_id WHERE Medicos.id = :medico_id', {
        model: Pacientes,
        replacements: {
          medico_id: `${req.params.id}`
        },
        type: app.db.sequelize.QueryTypes.SELECT
      })
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/agendas')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      app.db.sequelize.query('SELECT Medicos.nombre AS `medico`, Pacientes.nombre AS `nombre`, Pacientes.domicilio AS `domicilio`, Pacientes.foraneo AS `foraneo`, Pacientes.telefono AS `telefono`, `Pacientes.Citas`.`id` AS `id`, `Pacientes.Citas`.`consulta` AS `consulta`, `Pacientes.Citas`.`observaciones` AS `observaciones`, `Pacientes.Citas`.`fecha` AS `fecha`, `Pacientes.Citas`.`hora` AS `hora`, `Pacientes.Citas`.`confirmada` AS `confirmada`, Usuarios.usuario AS `usuario` FROM Medicos INNER JOIN (`Citas` AS `Pacientes.Citas` INNER JOIN Pacientes ON Pacientes.id = `Pacientes.Citas`.`paciente_id` INNER JOIN Usuarios ON Usuarios.id = `Pacientes.Citas`.`usuario_id`) ON Medicos.id = `Pacientes.Citas`.`medico_id` ORDER BY Medicos.id ASC, `Pacientes.Citas`.`fecha` DESC, `Pacientes.Citas`.`hora` ASC', {
        model: Pacientes,
        type: app.db.sequelize.QueryTypes.SELECT
      })
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/agendas/:fecha')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      app.db.sequelize.query('SELECT Medicos.id AS `medico_id`, Pacientes.nombre AS `nombre`, Pacientes.domicilio AS `domicilio`, Pacientes.foraneo AS `foraneo`, Pacientes.telefono AS `telefono`, Pacientes.alergias AS `alergias`, `Pacientes.Citas`.`id` AS `id`, `Pacientes.Citas`.`consulta` AS `consulta`, `Pacientes.Citas`.`fecha` AS `fecha`, `Pacientes.Citas`.`hora` AS `hora`, `Pacientes.Citas`.`confirmada` AS `confirmada`, `Pacientes.Citas`.`presente` AS `presente`, `Pacientes.Citas`.`observaciones` AS `observaciones` FROM Medicos INNER JOIN (`Citas` AS `Pacientes.Citas` INNER JOIN Pacientes ON Pacientes.id = `Pacientes.Citas`.`paciente_id`) ON Medicos.id = `Pacientes.Citas`.`medico_id` WHERE `Pacientes.Citas`.`fecha` LIKE DATE:fecha ORDER BY `Pacientes.Citas`.`hora` ASC', {
        model: Pacientes,
        replacements: {
          fecha: `${req.params.fecha}`
        },
        type: app.db.sequelize.QueryTypes.SELECT
      })
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });
};
