/*
 * File: citas.js
 *
 * Created: 01 jun 2018 21:05:40
 * Last Modified: 01 jun 2018 21:05:40
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = app => {
  const Citas = app.db.models.Citas;

  app.route('/citas')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      Citas.findAll({})
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .post((req, res) => {
      Citas.create(req.body)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/citas/:id')
    .get((req, res) => {
      Citas.findOne({ where: req.params })
        .then(result => {
          if (result) {
            res.json(result);
          }
          else {
            res.sendStatus(404);
          }
        })
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .put((req, res) => {
      Citas.update(req.body, { where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .delete((req, res) => {
      Citas.destroy({ where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });
};
