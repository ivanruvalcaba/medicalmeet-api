/*
 * File: usuarios.js
 *
 * Created: 04 jul 2018 12:19:47
 * Last Modified: 04 jul 2018 22:10:34
 *
 * Copyright (c) 2018, Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 * Author: Iván Ruvalcaba <mario.i.ruvalcaba[at]gmail[dot]com>
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License,
 * v. 2.0. If a copy of the MPL was not distributed with this file, You can
 * obtain one at http://mozilla.org/MPL/2.0/.
 */

module.exports = app => {
  const Usuarios = app.db.models.Usuarios;

  app.route('/usuarios')
    .get((req, res) => {  // eslint-disable-line no-unused-vars
      Usuarios.findAll({})
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .post((req, res) => {
      Usuarios.create(req.body)
        .then(result => res.json(result))
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/usuarios/:id')
    .get((req, res) => {
      Usuarios.findOne({ where: req.params })
        .then(result => {
          if (result) {
            res.json(result);
          }
          else {
            res.sendStatus(404);
          }
        })
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .put((req, res) => {
      Usuarios.update(req.body, { where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    })
    .delete((req, res) => {
      Usuarios.destroy({ where: req.params })
        .then(result => res.sendStatus(204))  // eslint-disable-line no-unused-vars
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });

  app.route('/login/:usuario')
    .get((req, res) => {
      Usuarios.findOne({ where: req.params })
        .then(result => {
          if (result) {
            res.json(result);
          }
          else {
            res.sendStatus(404);
          }
        })
        .catch(error => {
          res.status(412).json({
            msg: error.message
          });
        });
    });
};
